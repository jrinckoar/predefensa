function [fig1,fig2]=EEGMaxSNR5()
clear;
clc;
addpath ~/Documents/PhD/inv/ApEn/exps/exp_11/
fpath='~/Copy/PhD_Data/ApEn/Exp_11/';
[pout,ApEnt_p1_n4,ApEnt_p2_n4,ApEnt_p3_n4,ApEnt_p4_n4]=...
    exp_11_loadData(fpath,'ApEnt_p1_n4','ApEnt_p2_n4',...
    'ApEnt_p3_n4','ApEnt_p4_n4');

p=1:4;
w=pout{1}.w;
N=pout{1}.N;
tau=pout{1}.tau;
m=pout{1}.m;
rmax=pout{1}.rmax;
r=linspace(0,rmax,800);


[N,M]=size(ApEnt_p1_n4);

for i=1:M
    [temp,ind]=max(ApEnt_p1_n4(:,i));
    Max_ApEnt_p1_n4(i)=temp;
    Max_r_p1_n4(i)=r(ind);
    
    [temp,ind]=max(ApEnt_p2_n4(:,i));
    Max_ApEnt_p2_n4(i)=temp;
    Max_r_p2_n4(i)=r(ind);
    
    [temp,ind]=max(ApEnt_p3_n4(:,i));
    Max_ApEnt_p3_n4(i)=temp;
    Max_r_p3_n4(i)=r(ind);

    [temp,ind]=max(ApEnt_p4_n4(:,i));
    Max_ApEnt_p4_n4(i)=temp;
    Max_r_p4_n4(i)=r(ind);
end

fig1=figure();
plot(m,Max_ApEnt_p1_n4,'v-k','LineWidth',3',...
    'MarkerFaceColor','k','MarkerSize',6);
hold on;
plot(m,Max_ApEnt_p3_n4,'v-','Color',[0.5,0.5,0.5],'LineWidth',3,...
    'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',6);
plot(m,Max_ApEnt_p2_n4,'-sk','LineWidth',3',...
    'MarkerFaceColor','k','MarkerSize',6);
plot(m,Max_ApEnt_p4_n4,'s-','Color',[0.5,0.5,0.5],'LineWidth',3,...
    'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',6);
hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica')
ylabel('ApEn_{max}','FontSize',22, 'FontName', 'Helvetica')
x(1)=1.5;
x(2)=20.5;
y(1)=0;
y(2)=2.2;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20);
set(gca,'YTick',y(1):ys:y(2));
l=legend('Pre-Ictal 1','Ictal 1','Pre-Ictal 2','Ictal 2','Location','NorthEast');
set(l,'FontSize',22);
grid on;

fig2=figure();
plot(m,Max_r_p1_n4,'v-k','LineWidth',3',...
    'MarkerFaceColor','k','MarkerSize',6);
hold on;
plot(m,Max_r_p3_n4,'v-','Color',[0.5,0.5,0.5],'LineWidth',3,...
    'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',6);
plot(m,Max_r_p2_n4,'-sk','LineWidth',3',...
    'MarkerFaceColor','k','MarkerSize',6);
plot(m,Max_r_p4_n4,'s-','Color',[0.5,0.5,0.5],'LineWidth',3,...
    'MarkerFaceColor',[0.5 0.5 0.5],'MarkerSize',6);
hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica')
ylabel('h_{max}','FontSize',22, 'FontName', 'Helvetica')
x(1)=1.5;
x(2)=20.5;
y(1)=0;
y(2)=0.04;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20);
set(gca,'YTick',y(1):ys:y(2));
l=legend('Pre-Ictal 1','Ictal 1','Pre-Ictal 2','Ictal 2','Location','SouthEast');
set(l,'FontSize',22);
grid on;
dock all
end


