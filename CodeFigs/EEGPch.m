function [fig1,fig2]=EEGPch()
clear;
clc;
N=2;
SNR=2;

% Load Data
fpath='/home/jrestrepo/Copy/PhD_Data/ApEn/Exp_46/';
addpath /home/jrestrepo/Documents/PhD/inv/ApEn/exps/exp_46/;
[pout,Data]=exp_46_loadData(fpath,'all',['SNR',num2str(SNR),'_N',num2str(N)]);

ApEn_max=Data.ApEn_max;
r_max=Data.r_max;
eeg=Data.eeg;
noise=Data.Noise;
w=pout{1,1}.w;

% Signal
signal=eeg+noise*std(eeg)*w;
% x-axis
step=pout{1,1}.step;
[M,K]=size(ApEn_max);
tw=0:step:(K*step-1);

% Statistical Normalization
ApEn_max=(ApEn_max-repmat(mean(ApEn_max,2),1,K))./repmat(std(ApEn_max,[],2),1,K);
r_max=(r_max-repmat(mean(r_max,2),1,K))./repmat(std(r_max,[],2),1,K);
CA=ApEn_max'*ApEn_max/M;
CR=r_max'*r_max/M;
AR=[ApEn_max;r_max]'*[ApEn_max;r_max]/2*M;

% PCA
[V,D]=eig(CA);
ind= diag(D)==max(diag(D));
ApEn_PCA=V(:,ind);
 
[V,D]=eig(CR);
ind= diag(D)==max(diag(D));
r_PCA=V(:,ind);

[V,D]=eig(AR);
ind= diag(D)==max(diag(D));
AR_PCA=V(:,ind);

% CUSUM
c=20;
k=2;

ApEn_mean=mean(ApEn_PCA(1:c));
ApEn_std=std(ApEn_PCA(1:c))*k;
r_mean=mean(r_PCA(1:c));
r_std=std(r_PCA(1:c))*k;
AR_mean=mean(AR_PCA(1:c));
AR_std=std(AR_PCA(1:c))*k;

ApEn_CUSUM(1:c)=0;
r_CUSUM(1:c)=0;
AR_CUSUM(1:c)=0;
for i=c:length(ApEn_PCA)
    ApEn_CUSUM(i)=max(0,ApEn_PCA(i)-(ApEn_mean+ApEn_std) + ApEn_CUSUM(i-1));
    r_CUSUM(i)=max(0,r_PCA(i)-(r_mean+r_std) + r_CUSUM(i-1));
    AR_CUSUM(i)=max(0,AR_PCA(i)-(AR_mean+AR_std) + AR_CUSUM(i-1));
end

% Scale for plot
signal=signal./max(signal);
AR_PCA=AR_PCA./max(AR_PCA);
scale=max([max(ApEn_CUSUM),max(r_CUSUM),max(AR_CUSUM)]);
ApEn_CUSUM=ApEn_CUSUM./scale;
r_CUSUM=r_CUSUM./scale;
AR_CUSUM=AR_CUSUM./scale;


% Ictal episodes marks
tc=[5000;8000;10000;12000;15000;17000;21400;24400];

% Figure Axes
ax1=[0 25000 -1 1.1];
%ax2=[0 25000 -1 1];
ax3=[0 25000 -0.01 1.1];

% Plot EEG
fig1=figure();
plot(signal,'Color',[0.6,0.6,0.6],'LineWidth',3);
hold on;
temp=ax1(3):0.001:ax1(4);
for i=1:length(tc)
    temp2=repmat(tc(i),1,length(temp));
    plot(temp2,temp,'-.','Color',[0.2,0.2,0.2],'LineWidth',3);
end
hold off;
axis(ax1);
set(gca,'Xtick',[]);
y1=ax1(3);
y2=ax1(4)-0.1;
ys=(abs(y2-y1))/5;
set(gca,'YTick',y1:ys:y2);
grid on;
set(gca,'XGrid','off');
ylabel('EEG','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')

fig2=figure();
p1=plot(tw,AR_CUSUM,'Color',[0.0,0.0,0.0],'LineWidth',3);
hold on;
p2=plot(tw,ApEn_CUSUM,'Color',[0.5,0.5,0.5],'LineWidth',3);
p3=plot(tw,r_CUSUM,'--','Color',[0.5,0.5,0.5],'LineWidth',3);
temp=ax1(3):0.001:ax1(4);
for i=1:length(tc)
    temp2=repmat(tc(i),1,length(temp));
    plot(temp2,temp,'-.','Color',[0.2,0.2,0.2],'LineWidth',3);
end
hold off
axis(ax3);
xlabel('n','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
set(gca,'Xtick',tc);
y1=ax3(3)+0.01;
y2=ax3(4)-0.1;
ys=(abs(y2-y1))/5;
set(gca,'YTick',0:ys:y2);
grid on;
set(gca,'XGrid','off');
ylabel('CUSUM','FontSize',22, 'FontName', 'Helvetica')
l=legend([p2 p3 p1],{'$\,\mathbf{ApEn_{max}}$','$\,\mathbf{h_{max}}$','$\,\mathbf{ApEn_{max}\, \&\, h_{max}}$'},...
    'Location','NorthWest');
 set(l,'FontSize',18,'Interpreter','Latex');
dock all;

% fig2=figure();
% plot(tw,AR_PCA,'Color',[0.0,0.0,0.0],'LineWidth',2);
% hold on;
% temp=ax1(3):0.001:ax1(4);
% for i=1:length(tc)
%     temp2=repmat(tc(i),1,length(temp));
%     plot(temp2,temp,'-.','Color',[0.2,0.2,0.2],'LineWidth',3);
% end
% hold off;
% axis(ax2);
% set(gca,'Xtick',[]);
% y1=ax2(3);
% y2=ax2(4);
% ys=(abs(y2-y1))/5;
% set(gca,'YTick',y1:ys:y2);
% grid on;
% set(gca,'XGrid','off');
% ylabel('1^{st} PC','FontSize',20, 'FontName', 'Helvetica')

end
