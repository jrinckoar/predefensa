function [fig1,fig2,fig3]=LogMap_EvsN()

fpath='/home/jrestrepo/Copy/PhD_Data/ApEn/Exp_44/LOUCV/';
SNR=[1;2;3];
N=[1;2;3;4;5;6;7;8];
Nt=[500;1000;2000;3000;4000;5000;8000;10000];

for i=1:length(SNR)
    str1=['SNR',num2str(SNR(i))];
    flag=1;
    for j=1:length(N)
        fname=[fpath,str1,'/','Exp44CV_SNR',num2str(SNR(i)),'_N',num2str(N(j)),'.mat'];
        y=load(fname);
        m=y.('Exp_opt').('m');
        if(flag)
            for k=1:length(m)
                str2=['m',num2str(m(k))];
                ind.(str2)=1;
            end
            flag=0;
        end
        for k=1:length(m)
            str2=['m',num2str(m(k))];
            temp=ind.(str2);
            MissRate.(str1).(str2)(temp)=y.('MissRate').(str2);
            n.(str1).(str2)(temp)=y.('Exp_opt').('N');
            ind.(str2)=ind.(str2)+1;
        end
    end
end

fig1=figure(1);
str1=['SNR',num2str(SNR(1))];
%-------------------------------------------------------------------------
str2=['m',num2str(m(3))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(temp2);
p1=plot(temp1,temp2,'Color',[0.0,0.0,0.0],'LineWidth',3);
hold on;
%-------------------------------------------------------------------------
str2=['m',num2str(m(4))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=[0.5 0.5 0.5];
p2=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', 's','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',8.0);
%-------------------------------------------------------------------------
str2=['m',num2str(m(1))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p3=plot(temp1,temp2,'Color',[0.5,0.5,0.5],'LineWidth',3);
%-------------------------------------------------------------------------
str2=['m',num2str(m(2))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=[0.0 0.0 0.0];
p4=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', '.','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',20.0);
hold off;
%-------------------------------------------------------------------------
l=legend([p3 p4 p1 p2],{['$m=' num2str(m(1)) '$'],['$m=' num2str(m(2)) '$'],...
    ['$m=' num2str(m(3)) '$'],['$m=' num2str(m(4)) '$']},'Location','NorthEast');
xlabel('N','FontSize',20, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('Tasa de Error','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
x(1)=300;
x(2)=10100;
z(1)=-0.01;
z(2)=0.35;
axis ([x(1) x(2) z(1) z(2)]);
ys=(z(2)-z(1))/5;
scale=1E3;
set(gca,'XTick',Nt)
set(gca,'XTickLabel',num2str(get(gca,'XTick').'/scale))
annotation(fig1,'textbox',...
    [0.833137485311398 0.0196802884257259 0.0865955469535997 0.0428616122640525],...
    'String',{'\times10^3'},...
    'HorizontalAlignment','right',...
    'FontSize',16,'FontName','Helvetica',...
    'FitBoxToText','off',...
    'LineStyle','none');
set(gca,'YTick',a:ys:z(2))
set(l,'FontSize',22,'Interpreter','Latex');
grid on;


fig2=figure(2);
str1=['SNR',num2str(SNR(2))];
%-------------------------------------------------------------------------
str2=['m',num2str(m(3))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(temp2);
p1=plot(temp1,temp2,'Color',[0.0,0.0,0.0],'LineWidth',3);
hold on;
%-------------------------------------------------------------------------
str2=['m',num2str(m(4))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=[0.5 0.5 0.5];
p2=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', 's','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',8.0);
%-------------------------------------------------------------------------
str2=['m',num2str(m(1))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p3=plot(temp1,temp2,'Color',[0.5,0.5,0.5],'LineWidth',3);
%-------------------------------------------------------------------------
str2=['m',num2str(m(2))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=[0.0 0.0 0.0];
p4=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', '.','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',20.0);
hold off;
%-------------------------------------------------------------------------
l=legend([p3 p4 p1 p2],{['$m=' num2str(m(1)) '$'],['$m=' num2str(m(2)) '$'],...
    ['$m=' num2str(m(3)) '$'],['$m=' num2str(m(4)) '$']},'Location','NorthEast');
xlabel('N','FontSize',20, 'FontName', 'Helvetica','Interpreter','Latex')
x(1)=300;
x(2)=10100;
z(1)=-0.01;
z(2)=0.35;
% z(1)=-0.01;
% z(2)=0.4;
axis ([x(1) x(2) z(1) z(2)]);
ys=(z(2)-z(1))/5;
scale=1E3;
set(gca,'XTick',Nt)
set(gca,'XTickLabel',num2str(get(gca,'XTick').'/scale))
annotation(fig2,'textbox',...
    [0.833137485311398 0.0196802884257259 0.0865955469535997 0.0428616122640525],...
    'String',{'\times 10^3'},...
    'HorizontalAlignment','right',...
    'FontSize',16,...
    'FitBoxToText','off',...
    'LineStyle','none');
set(gca,'YTick',a:ys:z(2))
set(l,'FontSize',22,'Interpreter','Latex');
grid on;

fig3=figure(3);
str1=['SNR',num2str(SNR(3))];
%-------------------------------------------------------------------------
str2=['m',num2str(m(3))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(temp2);
p1=plot(temp1,temp2,'Color',[0.0,0.0,0.0],'LineWidth',3);
hold on;
%-------------------------------------------------------------------------
str2=['m',num2str(m(4))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=[0.5 0.5 0.5];
p2=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', 's','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',8.0);
%-------------------------------------------------------------------------
str2=['m',num2str(m(1))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p3=plot(temp1,temp2,'Color',[0.5,0.5,0.5],'LineWidth',3);
%-------------------------------------------------------------------------
str2=['m',num2str(m(2))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=[0.0 0.0 0.0];
p4=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', '.','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',20.0);
hold off;
%-------------------------------------------------------------------------
l=legend([p3 p4 p1 p2],{['$m=' num2str(m(1)) '$'],['$m=' num2str(m(2)) '$'],...
    ['$m=' num2str(m(3)) '$'],['$m=' num2str(m(4)) '$']},'Location','NorthEast');
xlabel('N','FontSize',20, 'FontName', 'Helvetica','Interpreter','Latex')
x(1)=300;
x(2)=10100;
z(1)=-0.01;
z(2)=0.35;
%z(1)=-0.01;
%z(2)=0.4;
axis ([x(1) x(2) z(1) z(2)]);
ys=(z(2)-z(1))/5;
set(gca,'XTick',Nt);
scale=1E3;
set(gca,'XTickLabel',num2str(get(gca,'XTick').'/scale))
annotation(fig3,'textbox',...
    [0.833137485311398 0.0196802884257259 0.0865955469535997 0.0428616122640525],...
    'String',{'\times 10^3'},...
    'HorizontalAlignment','right',...
    'FontSize',16,...
    'FitBoxToText','off',...
    'LineStyle','none');
set(gca,'YTick',a:ys:z(2))
set(l,'FontSize',22,'Interpreter','Latex');
grid on;

dock all;

end
