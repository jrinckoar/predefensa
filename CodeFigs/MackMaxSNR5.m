function [fig1,fig2]=MackMaxSNR5(opt)
clc;
Exp=2;
SNR=['SNR' num2str(Exp)];
addpath ~/Documents/PhD/inv/ApEn/exps/exp_41/; 
fpath=['~/Copy/PhD_Data/ApEn/Exp_41/',SNR, '/'];

[pout1,Data1]=exp_41_loadData(fpath,'max',[SNR '_p1']);
[pout2,Data2]=exp_41_loadData(fpath,'max',[SNR '_p2']);

p(1)=pout1{1,1}.c;
p(2)=pout2{1,1}.c;
m=pout1{1,1}.m;

ApEn1=Data1.('ApEn_max')';
ApEn2=Data2.('ApEn_max')';
r1=Data1.('r_max')';
r2=Data2.('r_max')';
[K,~]=size(ApEn1);


if strcmp(opt,'CI')
alpha=0.05;

MApEn_1=mean(ApEn1);
MApEn_2=mean(ApEn2);
Mr_1=mean(r1);
Mr_2=mean(r2);

temp=sort(ApEn1);
CiL_ApEn_1=temp(floor(K*alpha/2),:);
CiU_ApEn_1=temp(floor((1-alpha/2)*K),:);
CiL_ApEn_1=abs(CiL_ApEn_1-MApEn_1);
CiU_ApEn_1=abs(CiU_ApEn_1-MApEn_1);

temp=sort(ApEn2);
CiL_ApEn_2=temp(floor(alpha/2*K),:);
CiU_ApEn_2=temp(floor((1-alpha/2)*K),:);
CiL_ApEn_2=abs(CiL_ApEn_2-MApEn_2);
CiU_ApEn_2=abs(CiU_ApEn_2-MApEn_2);

temp=sort(r1);
CiL_r_1=temp(floor(K*alpha/2),:);
CiU_r_1=temp(floor((1-alpha/2)*K),:);
CiL_r_1=abs(CiL_r_1-Mr_1);
CiU_r_1=abs(CiU_r_1-Mr_1);

temp=sort(r2);
CiL_r_2=temp(floor(alpha/2*K),:);
CiU_r_2=temp(floor((1-alpha/2)*K),:);
CiL_r_2=abs(CiL_r_2-Mr_2);
CiU_r_2=abs(CiU_r_2-Mr_2);


fig1=figure(3);
errorbar(m,MApEn_1,CiL_ApEn_1,CiU_ApEn_1,'r','LineWidth',3);hold on;
errorbar(m,MApEn_2,CiL_ApEn_2,CiU_ApEn_2,'Color',[0,0,1],'LineWidth',3);hold off;
xlabel('m','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{ApEn_{max}}$','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=0;
y(2)=2.2;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',y(1):ys:y(2))
l=legend(['$\,c = ' num2str(p(1)) '$'],['$\,c = ' num2str(p(2)) '$']);
set(l,'FontSize',30,'Interpreter','Latex');
grid on;

fig2=figure(4);
errorbar(m,Mr_1,CiL_r_1,CiU_r_1,'r','LineWidth',3);hold on;
errorbar(m,Mr_2,CiL_r_2,CiU_r_2,'Color',[0,0,1],'LineWidth',3);hold off;
xlabel('m','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{h_{max}}$','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=0;
y(2)=0.024;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',y(1):ys:y(2))
l=legend(['$\,c = ' num2str(p(1)) '$'],['$\,c = ' num2str(p(2)) '$'],...
    'Location','NorthWest');
set(l,'FontSize',30,'Interpreter','Latex');
grid on;
dock all;

else


SApEn_1=std(ApEn1);
SApEn_2=std(ApEn2);

MApEn_1=mean(ApEn1);
MApEn_2=mean(ApEn2);

Sr_1=std(r1);
Sr_2=std(r2);
Mr_1=mean(r1);
Mr_2=mean(r2);

fig1=figure();
errorbar(m,MApEn_1,SApEn_1,'r','LineWidth',3);hold on;
errorbar(m,MApEn_2,SApEn_2,'Color',[0,0,1],'LineWidth',3);hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{ApEn_{max}}$','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=0;
y(2)=2.2;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',y(1):ys:y(2))
l=legend(['$\,a = ' num2str(p(1)) '$'],['$\,a = ' num2str(p(2)) '$']);
set(l,'FontSize',22,'Interpreter','Latex');
grid on;

fig2=figure();
errorbar(m,Mr_1,Sr_1,'r','LineWidth',3);hold on;
errorbar(m,Mr_2,Sr_2,'Color',[0,0,1],'LineWidth',3);hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{h_{max}}$','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=0;
y(2)=0.024;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',y(1):ys:y(2))
l=legend(['$\,a = ' num2str(p(1)) '$'],['$\,a = ' num2str(p(2)) '$'],...
    'Location','NorthWest');
set(l,'FontSize',22,'Interpreter','Latex');
grid on;
dock all;
end
end
