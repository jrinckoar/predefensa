function [fig1,fig2,fig3]=Mack_CV()

fpath='~/Copy/PhD_Data/ApEn/Exp_41/CV/';
CVnfold_label='MR10fold';

% SNR = Inf db
SNR=1;
foname=[fpath,'Exp41_CV_SNR',num2str(SNR), '.mat'];
load(foname);
m=CV.pout{1}{1}.m;
MR_A=CV.(CVnfold_label).('MisR_ApEn');
MR_R=CV.(CVnfold_label).('MisR_r');
MR_B=CV.(CVnfold_label).('MisR_both');

fig1=figure();
p1=plot(m,MR_B,'Color',[0.0,0.0,0.0],'LineWidth',4);
hold on;
p2=plot(m,MR_A,'Color',[0.5,0.5,0.5],'LineWidth',3,'LineStyle','-');
p3=plot(m,MR_R,'Color',[0.5,0.5,0.5],'LineStyle','--','LineWidth',3);
hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('Tasa de Error','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=-0.01;
y(2)=0.5;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',min(MR_B):ys:y(2))
l=legend([p2 p3 p1],{'$\,\mathbf{ApEn_{max}}$','$\,\mathbf{h_{max}}$','$\,\mathbf{ApEn_{max}\, \&\, h_{max}}$'},...
    'Location','NorthWest');
set(l,'FontSize',22,'Interpreter','Latex');
grid on;


% SNR = 5 db
SNR=2;
foname=[fpath,'Exp41_CV_SNR',num2str(SNR), '.mat'];
load(foname);
m=CV.pout{1}{1}.m;
MR_A=CV.(CVnfold_label).('MisR_ApEn');
MR_R=CV.(CVnfold_label).('MisR_r');
MR_B=CV.(CVnfold_label).('MisR_both');

fig2=figure();
p1=plot(m,MR_B,'Color',[0.0,0.0,0.0],'LineWidth',4);
hold on;
p2=plot(m,MR_A,'Color',[0.5,0.5,0.5],'LineWidth',3,'LineStyle','-');
p3=plot(m,MR_R,'Color',[0.5,0.5,0.5],'LineStyle','--','LineWidth',3);
hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
%ylabel('Misclassification Rate','FontSize',20, 'FontName', 'Helvetica')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=-0.01;
y(2)=0.5;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',min(MR_B):ys:y(2))
l=legend([p2 p3 p1],{'$\,\mathbf{ApEn_{max}}$','$\,\mathbf{h_{max}}$','$\,\mathbf{ApEn_{max}\, \&\, h_{max}}$'},...
    'Location','NorthWest');
set(l,'FontSize',22,'Interpreter','Latex');
grid on;


% SNR = 0 db
SNR=4;
foname=[fpath,'Exp41_CV_SNR',num2str(SNR), '.mat'];
load(foname);
m=CV.pout{1}{1}.m;
MR_A=CV.(CVnfold_label).('MisR_ApEn');
MR_R=CV.(CVnfold_label).('MisR_r');
MR_B=CV.(CVnfold_label).('MisR_both');

fig3=figure();
p1=plot(m,MR_B,'Color',[0.0,0.0,0.0],'LineWidth',4);
hold on;
p2=plot(m,MR_A,'Color',[0.5,0.5,0.5],'LineWidth',3,'LineStyle','-');
p3=plot(m,MR_R,'Color',[0.5,0.5,0.5],'LineStyle','--','LineWidth',3);
hold off;
xlabel('m','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
%ylabel('Misclassification Rate','FontSize',20, 'FontName', 'Helvetica')
grid on;
x(1)=1.5;
x(2)=20.5;
y(1)=-0.01;
y(2)=0.5;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
set(gca,'XTick',0:2:20)
set(gca,'YTick',min(MR_B):ys:y(2))
l=legend([p2 p3 p1],{'$\,\mathbf{ApEn_{max}}$','$\,\mathbf{h_{max}}$','$\,\mathbf{ApEn_{max}\, \&\, h_{max}}$'},...
    'Location','NorthWest');
set(l,'FontSize',22,'Interpreter','Latex');
grid on;
dock all;
end
