function [fig1,fig2,fig3]=Mack_EvsN()

close all;

fpath='/home/jrestrepo/Copy/PhD_Data/ApEn/Exp_45/LOUCV/';
SNR=[1;2;3];
N=[1;2;3;4;5;6;7;8];
Nt=[1000;2000;3000;5000;8000;10000;15000;20000];

str1=['SNR',num2str(SNR(end))];
fname=[fpath,str1,'/','Exp45CV_SNR',num2str(SNR(end)),'_N',num2str(N(end)),'.mat'];
y=load(fname);
M=y.('Exp_opt').('m');
for k=1:length(M)
    str2=['m',num2str(M(k))];
    ind.(str2)=1;
end
for i=1:length(SNR)
    str1=['SNR',num2str(SNR(i))];
    for j=1:length(N)
        fname=[fpath,str1,'/','Exp45CV_SNR',num2str(SNR(i)),'_N',num2str(N(j)),'.mat'];
        y=load(fname);
        m=y.('Exp_opt').('m');
        for k=1:length(m)
            str2=['m',num2str(m(k))];
            temp=ind.(str2);
            MissRate.(str1).(str2)(temp)=y.('MissRate').(str2);
            n.(str1).(str2)(temp)=y.('Exp_opt').('N');
            ind.(str2)=ind.(str2)+1;
        end
    end
    for k=1:length(M)
        str2=['m',num2str(M(k))];
        ind.(str2)=1;
    end
end



func = @(x) colorspace('RGB->Lab',x);
c = distinguishable_colors(6,'w',func);

fig1=figure(1);
str1=['SNR',num2str(SNR(1))];
%-------------------------------------------------------------------------
str2=['m',num2str(m(3))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(temp2);
p1=plot(temp1,temp2,'Color',c(1,:),'LineWidth',4);
hold on;
%-------------------------------------------------------------------------
str2=['m',num2str(m(4))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p2=plot(temp1,temp2,'Color',c(2,:),'LineStyle','--','LineWidth',4,...
    'Marker', 's','MarkerFaceColor',c(2,:), 'MarkerEdgeColor',c(2,:),...
    'MarkerSize',8.0);
%-------------------------------------------------------------------------
str2=['m',num2str(m(1))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p3=plot(temp1,temp2,'Color',c(3,:),'LineWidth',4);
%-------------------------------------------------------------------------
str2=['m',num2str(m(2))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p4=plot(temp1,temp2,'Color',c(4,:),'LineStyle','--','LineWidth',4,...
    'Marker', '.','MarkerFaceColor',c(4,:), 'MarkerEdgeColor',c(4,:),...
    'MarkerSize',20.0);
hold off;
%-------------------------------------------------------------------------
l=legend([p3 p4 p1 p2],{['$m=' num2str(m(1)) '$'],['$m=' num2str(m(2)) '$'],...
    ['$m=' num2str(m(3)) '$'],['$m=' num2str(m(4)) '$']},'Location','NorthEast');
xlabel('N','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('Tasa de Error','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
x(1)=800;
x(2)=20100;
z(1)=-0.01;
z(2)=0.3;
axis ([x(1) x(2) z(1) z(2)]);
ys=(z(2)-z(1))/5;
scale=1E3;
set(gca,'XTick',Nt)
set(gca,'XTickLabel',num2str(get(gca,'XTick').'/scale))
annotation(fig1,'textbox',...
    [0.833137485311398 0.0196802884257259 0.0865955469535997 0.0428616122640525],...
    'String',{'\times10^3'},...
    'HorizontalAlignment','right',...
    'FontSize',22,'FontName','Helvetica',...
    'FitBoxToText','off',...
    'LineStyle','none');
set(gca,'YTick',a:ys:z(2))
set(l,'FontSize',30,'Interpreter','Latex');
grid on;
% 
fig2=figure();
str1=['SNR',num2str(SNR(2))];
%-------------------------------------------------------------------------
str2=['m',num2str(m(3))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(temp2);
p1=plot(temp1,temp2,'Color',c(1,:),'LineWidth',3);
hold on;
%-------------------------------------------------------------------------
str2=['m',num2str(m(4))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p2=plot(temp1,temp2,'Color',c(1,:),'LineStyle','--','LineWidth',3,...
    'Marker', 's','MarkerFaceColor',c(1,:), 'MarkerEdgeColor',c(1,:),...
    'MarkerSize',8.0);
%-------------------------------------------------------------------------
str2=['m',num2str(m(1))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p3=plot(temp1,temp2,'Color',c(2,:),'LineWidth',3);
%-------------------------------------------------------------------------
str2=['m',num2str(m(2))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p4=plot(temp1,temp2,'Color',c(2,:),'LineStyle','--','LineWidth',3,...
    'Marker', '.','MarkerFaceColor',c(2,:), 'MarkerEdgeColor',c(2,:),...
    'MarkerSize',20.0);
hold off;
%-------------------------------------------------------------------------
l=legend([p3 p4 p1 p2],{['$m=' num2str(m(1)) '$'],['$m=' num2str(m(2)) '$'],...
    ['$m=' num2str(m(3)) '$'],['$m=' num2str(m(4)) '$']},'Location','NorthEast');
xlabel('N','FontSize',22, 'FontName', 'Helvetica','Interpreter','Latex')
x(1)=800;
x(2)=20100;
z(1)=-0.01;
z(2)=0.3;
axis ([x(1) x(2) z(1) z(2)]);
ys=(z(2)-z(1))/5;
scale=1E3;
set(gca,'XTick',Nt)
set(gca,'XTickLabel',num2str(get(gca,'XTick').'/scale))
annotation(fig2,'textbox',...
    [0.833137485311398 0.0196802884257259 0.0865955469535997 0.0428616122640525],...
    'String',{'\times10^3'},...
    'HorizontalAlignment','right',...
    'FontSize',22,'FontName','Helvetica',...
    'FitBoxToText','off',...
    'LineStyle','none');
set(gca,'YTick',a:ys:z(2))
set(l,'FontSize',22,'Interpreter','Latex');
grid on;

fig3=figure(3);
str1=['SNR',num2str(SNR(3))];
%-------------------------------------------------------------------------
str2=['m',num2str(m(3))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(temp2);
p1=plot(temp1,temp2,'Color',c(1,:),'LineWidth',3);
hold on;
%-------------------------------------------------------------------------
str2=['m',num2str(m(4))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=c(2,:);
p2=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', 's','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',8.0);
%-------------------------------------------------------------------------
str2=['m',num2str(m(1))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
p3=plot(temp1,temp2,'Color',c(3,:),'LineWidth',3);
%-------------------------------------------------------------------------
str2=['m',num2str(m(2))];
temp1=n.(str1).(str2);
temp2=MissRate.(str1).(str2);
a=min(a,min(temp2));
ecolor=c(4,:);
p4=plot(temp1,temp2,'Color',ecolor,'LineStyle','--','LineWidth',3,...
    'Marker', '.','MarkerFaceColor',ecolor, 'MarkerEdgeColor',ecolor,...
    'MarkerSize',20.0);
hold off;
%-------------------------------------------------------------------------
l=legend([p3 p4 p1 p2],{['$m=' num2str(m(1)) '$'],['$m=' num2str(m(2)) '$'],...
    ['$m=' num2str(m(3)) '$'],['$m=' num2str(m(4)) '$']},'Location','NorthEast');
xlabel('N','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
x(1)=800;
x(2)=20100;
z(1)=-0.01;
z(2)=0.3;
axis ([x(1) x(2) z(1) z(2)]);
ys=(z(2)-z(1))/5;
set(gca,'XTick',Nt)
set(gca,'XTickLabel',num2str(get(gca,'XTick').'/scale))
annotation(fig3,'textbox',...
    [0.833137485311398 0.0196802884257259 0.0865955469535997 0.0428616122640525],...
    'String',{'\times10^3'},...
    'HorizontalAlignment','right',...
    'FontSize',22,'FontName','Helvetica',...
    'FitBoxToText','off',...
    'LineStyle','none');
set(gca,'YTick',a:ys:z(2))
set(l,'FontSize',30,'Interpreter','Latex');
grid on;

dock all;

end











