function [fig1]=SRCorInt()
clc;
fname='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/CodeFigs/Test_Scaling_N3000_w0.mat';
load(fname);

func = @(x) colorspace('RGB->Lab',x);
c = distinguishable_colors(4,'w',func);

fig1=figure(1);
plot(log(r),log(SRCI(:,1)),'Color',c(2,:),'LineWidth',4);
hold on;
plot(log(r),log(GKCI(:,1)),'-.','Color',c(3,:),'MarkerSize',5,'LineWidth',3);
plot(log(r),log(SRCI(:,2:end)),'Color',c(2,:),'LineWidth',4);
plot(log(r),log(GKCI(:,2:end)),'-.','Color',c(3,:),'MarkerSize',5,'LineWidth',3);

hold off
xlabel('$$\mathbf{\ln\left(h\right)}$$','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$$\mathbf{\ln\left[C_{\!m}\left(h\right)\right]}$$','FontSize',30, 'FontName', 'Helvetica','Interpreter','latex')

x(1)=-6;
x(2)=-2.40;
y(1)=-13;
y(2)=-4.2;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
xs=(x(2)-x(1))/5;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
l=legend('$$\,\mathbf{C_{\!m}}$$~:~~Algoritmo RE',...
    '$$\,\mathbf{Gk_{\!m}}$$:~Algoritmo Diks','location','NorthWest');
set(l,'FontSize',30,'Interpreter','Latex');
grid on;
text(-5.0, -6.5,'$$\mathbf{m=2}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(-5.0, -8.5,'$$\mathbf{m=3}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(-5.0, -10.2,'$$\mathbf{m=4}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');


dock;


end
