function [fig1,fig2,fig3]=SinChirpWn()
addpath /home/jrestrepo/Documents/PhD/inv/ApEn/


N = 5000;
t = linspace(0,2*pi,N);
S = sin(pi.*t.*10);
f = 5*t+5;
C = sin(pi.*t.*f);
W = randn(size(S));

S1=(S-mean(S))/std(S);
C1=(C-mean(C))/std(C);
W1=(W-mean(W))/std(W);

m      = 5;
r_max  = 1;
tau    = 1;
r      = linspace(0,r_max,1000);
bias   = 1;
Single = 1;
dist   = 0;

ApEnS1=ApEntropy(S1,'m0',m,'r0',r,'N',N,'tau0',tau,'bias',bias,'Single',Single,'dist',dist);
ApEnC1=ApEntropy(C1,'m0',m,'r0',r,'N',N,'tau0',tau,'bias',bias,'Single',Single,'dist',dist);
ApEnW1=ApEntropy(W1,'m0',m,'r0',r,'N',N,'tau0',tau,'bias',bias,'Single',Single,'dist',dist);

fig1=figure();
plot(r,ApEnS1,'b','LineWidth',3);
hold on;
plot(r,ApEnC1,'r','LineWidth',3);
plot(r,ApEnW1,'k','LineWidth',3);
hold off;
xlabel('$\mathbf{h}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{ApEn}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=0;
x(2)=r_max;
y(1)=0;
y(2)=1.3;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
xs=(x(2)-x(1))/5;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
l=legend('Seno','Chirp lineal','Ruido blanco','Location','NorthWest');
set(l,'FontSize',22);
grid on;

[Max_ApEnS,ind]=max(ApEnS1);
Max_rS=r(ind);
[Max_ApEnC,ind]=max(ApEnC1);
Max_rC=r(ind);
[Max_ApEnW,ind]=max(ApEnW1);
Max_rW=r(ind);

fig2=figure();
plot(r,ApEnS1,'b','LineWidth',3);
hold on;
plot(r,ApEnC1,'r','LineWidth',3);
plot(r,ApEnW1,'k','LineWidth',3);
plot(Max_rS,Max_ApEnS,'.b','MarkerSize',30);
plot(Max_rC,Max_ApEnC,'.r','MarkerSize',30);
plot(Max_rW,Max_ApEnW,'.k','MarkerSize',30);
plot(linspace(0,Max_rS,N),repmat(Max_ApEnS,1,N),'--b','LineWidth',3);
plot(linspace(0,Max_rC,N),repmat(Max_ApEnC,1,N),'--r','LineWidth',3);
plot(linspace(0,Max_rW,N),repmat(Max_ApEnW,1,N),'--k','LineWidth',3);
hold off;
xlabel('$\mathbf{h}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{ApEn}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=0;
x(2)=r_max;
y(1)=0;
y(2)=1.3;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
xs=(x(2)-x(1))/5;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
l=legend('Seno','Chirp lineal','White Noise','Location','NorthWest');
set(l,'FontSize',22);
grid on;

% ruido

w=0.04;
S2=S+randn(size(S))*std(S)*w;
C2=C+randn(size(C))*std(C)*w;

S2=(S2-mean(S2))/std(S2);
C2=(C2-mean(C2))/std(C2);

ApEnS2=ApEntropy(S2,'m0',m,'r0',r,'N',N,'tau0',tau,'bias',bias,'Single',Single,'dist',dist);
ApEnC2=ApEntropy(C2,'m0',m,'r0',r,'N',N,'tau0',tau,'bias',bias,'Single',Single,'dist',dist);

[Max_ApEnS2,ind]=max(ApEnS2);
Max_rS2=r(ind);
[Max_ApEnC2,ind]=max(ApEnC2);
Max_rC2=r(ind);

fig3=figure();
plot(r,ApEnS2,'b','LineWidth',3);
hold on;
plot(r,ApEnC2,'r','LineWidth',3);
plot(Max_rS2,Max_ApEnS2,'.b','MarkerSize',30);
plot(Max_rC2,Max_ApEnC2,'.r','MarkerSize',30);
plot(linspace(0,Max_rS2,N),repmat(Max_ApEnS2,1,N),'--b','LineWidth',3);
plot(linspace(0,Max_rC2,N),repmat(Max_ApEnC2,1,N),'--r','LineWidth',3);
plot(repmat(Max_rS2,1,N),linspace(0,Max_ApEnS2,N),'--b','LineWidth',3);
plot(repmat(Max_rC2,1,N),linspace(0,Max_ApEnC2,N),'--r','LineWidth',3);
hold off;
xlabel('$\mathbf{h}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{ApEn}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
x(1)=0;
x(2)=0.12;
y(1)=0;
y(2)=0.55;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/5;
xs=(x(2)-x(1))/5;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
l=legend('Seno','Chirp lineal','Location','SouthEast');
set(l,'FontSize',22);
annotation(fig3,'textbox',...
    [0.668867345568916 0.782890921356395 0.2002442002442 0.0850277264325323],...
    'String',{'SNR$=30$ dB'},...
    'FontSize',22,...
    'FitBoxToText','on',...
    'EdgeColor',[1 1 1],...
    'BackgroundColor',[1 1 1],'Interpreter','Latex');

dock all;
end
