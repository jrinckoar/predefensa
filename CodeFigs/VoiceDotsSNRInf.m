function [fig1]=VoiceDotsSNRInf()
% addpath '/home/jrestrepo/Documents/PhD/inv/ApEn/exps/exp_47/';
% addpath '/home/jrestrepo/Documents/PhD/inv/ApEn/'
% 
% fpath='/home/jrestrepo/Copy/PhD_Data/ApEn/Exp_47_1/';
% 
% [~,Data.VS1] = exp47_loadData(fpath,'data','VS1');
% [~,Data.VS2] = exp47_loadData(fpath,'data','VS2');
% 
% for i = 1:2
%     feval(@()assignin('caller',['r_max',num2str(i)], Data.(['VS',num2str(i)]).('r_max')));
%     feval(@()assignin('caller',['ApEn_max',num2str(i)], Data.(['VS',num2str(i)]).('ApEn_max')));
% end

%clc;
addpath '/home/jrestrepo/Documents/PhD/inv/ApEn/exps/exp_47/';
addpath '/home/jrestrepo/Documents/PhD/inv/ApEn/'
fpath2='/home/jrestrepo/Copy/PhD_Data/ApEn/Exp_47_2/';

[pout1,Data.VS1] = exp47_loadData(fpath2,'data','VS1');
[pout2,Data.VS2] = exp47_loadData(fpath2,'data','VS2');
m1=pout1.m;
r1=pout1.r;

m2=pout2.m;
r2=pout2.r;

for i = 1:2
    feval(@()assignin('caller',['r_max',num2str(i)], Data.(['VS',num2str(i)]).('r_max')));
    feval(@()assignin('caller',['ApEn_max',num2str(i)], Data.(['VS',num2str(i)]).('ApEn_max')));
    feval(@()assignin('caller',['ApEn',num2str(i)], Data.(['VS',num2str(i)]).('ApEn')));
 
end

[~,M,K]=size(ApEn1);
ApEn_max1=zeros(K,M);
r_max1=zeros(K,M);
rt=0.01;
for k=1:K
    for j=1:M
        [~,ind]=min(abs(rt-r1));
        ApEn=ApEn1(1:ind,j,k);
        [temp,ind]=max(ApEn);
        ApEn_max1(k,j)=temp;    
        r_max1(k,j)=r1(ind);
    end
end


[~,M,K]=size(ApEn2);
ApEn_max2=zeros(K,M);
r_max2=zeros(K,M);
rt=0.025;
for k=1:K
    for j=1:M
        [~,ind]=min(abs(rt-r2));
        ApEn=ApEn2(1:ind,j,k);
        [temp,ind]=max(ApEn);
        ApEn_max2(k,j)=temp;    
        r_max2(k,j)=r2(ind);
    end
end

Normal= [r_max1,ApEn_max1];
Pathol= [r_max2,ApEn_max2];
[n1,~]=size(Normal);
[n2,~]=size(Pathol);
X=[Normal;Pathol];
Y=[-1*ones(n1,1);ones(n2,1);];


% LDA
W = LDA(X,Y);
L = [ones(n1+n2,1) X] * W';

% PCA
[COEFF,latent,~]=pcacov(cov(L));
PCA=L*COEFF(:,1:2);

% PCA
[COEFF,latent,~]=pcacov(cov(X));
PCA1=X*COEFF;
% LDA
W1 = LDA(PCA1,Y);
L1 = [ones(n1+n2,1) PCA1] * W1';







options.MaxIter=30000;
options.Display='off';
Model=svmtrain(PCA,Y,'options',options,'Autoscale','false');

w = sum(Model.SupportVectors.*repmat(Model.Alpha,1,2));
b = Model.Bias;
nfolds=n1+n2-1;
classifier='LSVM';
[CVResults]=Cross_Validation(PCA,Y,'nfolds',nfolds,'Classifier',classifier);
CVResults.ConfMat

[CVResults]=Cross_Validation(L1(:,1),Y,'nfolds',nfolds,'Classifier',classifier);
CVResults.ConfMat

%PCA=L1;


func = @(x) colorspace('RGB->Lab',x);
c = distinguishable_colors(6,'w',func);


ind1=1:n1;
ind2=n1+1:n1+n2;
fig1=figure(5);
plot(PCA(ind2,1),PCA(ind2,2),'o','LineWidth',3,'Color',c(1,:),...
    'MarkerSize',8,'MarkerFaceColor',c(1,:),'MarkerEdgeColor',c(1,:));
hold on;
plot(PCA(ind1,1),PCA(ind1,2),'o','LineWidth',3,'Color',c(2,:),...
    'MarkerSize',8,'MarkerFaceColor',c(2,:),'MarkerEdgeColor',c(2,:));
hold on;
grid on;
xlabel('$1^{ra}CP$','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$2^{da}CP$','FontSize',30, 'FontName', 'Helvetica','Interpreter','Latex')
xt(1)=90;
xt(2)=250;
yt(1)=11.5;
yt(2)=24;
t=linspace(xt(1),xt(2),1000);
plot(t,-b/w(2)-w(1)/w(2)*t,'--b','LineWidth',3,'Color',c(3,:));
hold off;
axis([xt(1) xt(2) yt(1) yt(2)]);
xs=(xt(2)-xt(1))/5;
ys=(yt(2)-yt(1))/5;
set(gca,'XTick',xt(1):xs:xt(2));
set(gca,'YTick',yt(1):ys:yt(2));
l=legend('Patol\''{o}gicas','Normales','Location','NorthWest');
set(l,'FontSize',30);
set(l,'Interpreter','Latex')
dock all;
end

