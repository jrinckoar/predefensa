%% Presentation Figures
%
clear;
close all;
addpath ~/Documents/PhD/inv/presentations/Predefensa/CodeFigs/;
%% Correlation dimension
%close all;
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=cdim();
exportf(f1,'Fig_1a',fpath);
exportf(f2,'Fig_1b',fpath);
exportf(f3,'Fig_1c',fpath);

%% Entropy
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=entropy();
%%
exportf(f1,'Fig_2a',fpath);
%%
exportf(f2,'Fig_2b',fpath);
exportf(f3,'Fig_2c',fpath);

%% Max Entropy
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=SinChirpWn();
exportf(f1,'Fig_3a',fpath);
exportf(f2,'Fig_3b',fpath);
exportf(f3,'Fig_3c',fpath);

%% Mack ApEn_max and r_max Inf SNR
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2]=MackMaxSNRInf('CI');
exportf(f1,'Fig_4a',fpath);
exportf(f2,'Fig_4b',fpath);
[f1,f2]=MackMaxSNR5('CI');
exportf(f1,'Fig_4c',fpath);
exportf(f2,'Fig_4d',fpath);

%% Mis. Rate vs m Shilnikov
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=Shil_CV();
exportf(f1,'Fig_5a',fpath);
exportf(f2,'Fig_5b',fpath);
exportf(f3,'Fig_5c',fpath);

%% Mis. Rate vs m MackeyG
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=Mack_CV();
exportf(f1,'Fig_5d',fpath);
exportf(f2,'Fig_5e',fpath);
exportf(f3,'Fig_5f',fpath);

%% Mis. Rate vs N Mackey
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=Mack_EvsN();
exportf(f1,'Fig_6a',fpath);
exportf(f2,'Fig_6b',fpath);
exportf(f3,'Fig_6c',fpath);

%% Mis. Rate vs N LogMap
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2,f3]=LogMap_EvsN();
exportf(f1,'Fig_6d',fpath);
exportf(f2,'Fig_6e',fpath);
exportf(f3,'Fig_6f',fpath);

%% EEG 
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2]=EEG();
exportf(f1,'Fig_7a',fpath);
exportf(f2,'Fig_7b',fpath);


%% EEG 
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2]=EEGMaxSNRInf();
exportf(f1,'Fig_7c',fpath);
exportf(f2,'Fig_7d',fpath);
% [f1,f2]=EEGMaxSNR5();
% exportf(f1,'Fig_7e',fpath);
% exportf(f2,'Fig_7f',fpath);

%% EEG Pch
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1,f2]=EEGPch();
exportf(f1,'Fig_8a',fpath);
exportf(f2,'Fig_8b',fpath);


%% Voice Dots
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1]=VoiceDotsSNRInf();
exportf(f1,'Fig_9a',fpath);


%% SR Corintegral
clear;
fpath='/home/jrestrepo/Documents/PhD/inv/presentations/Predefensa/Figs/';
[f1]=SRCorInt();
exportf(f1,'Fig_10a',fpath);
