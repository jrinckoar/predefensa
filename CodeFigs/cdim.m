function [fig1,fig2,fig3]=cdim()
clc;
addpath('~/Documents/PhD/inv/StochasticR/');

N=2000;
m      = 2;
tau    = 1;

% Load Data
y=load('~/Copy/PhD_Data/modelsData/HenonMap/henon_a1-4.mat','Henon');
% Henon Map
Henon  = y.('Henon')(500:500+N)';

% State Vectors
[V,~,~]=stateVec(Henon,m,tau);

%-------------------------------------------------------------------------------------
fig1=figure(1);
plot(Henon(600:750),'LineWidth',4)
xlabel('$\mathbf{n}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{X_{n}}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
axis tight;

%-------------------------------------------------------------------------------------

fig2=figure(2);
plot(V(:,2),V(:,1),'.','MarkerSize',20);
xlabel('$\mathbf{X_1}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{X_2}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
box on;
grid on;

%-------------------------------------------------------------------------------------

fig3=figure(3);
plot(V(:,2),V(:,1),'.','MarkerSize',20);
xlabel('$\mathbf{X_1}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
ylabel('$\mathbf{X_2}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex')
grid on;
H=gca;
set(H,'LineWidth',8)
set(H,'GridLineStyle', '-');
dock all;
end
