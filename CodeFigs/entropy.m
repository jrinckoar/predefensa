function [fig1,fig2,fig3]=entropy()
clc;
addpath('~/Documents/PhD/inv/StochasticR/');

N=2000;
m      = 2;
tau    = 1;

% Load Data
y=load('~/Copy/PhD_Data/modelsData/HenonMap/henon_a1-4.mat','Henon');
% Henon Map
Henon  = y.('Henon')(500:500+N)';
henon  = Henon(610:630);
clear y;
% State Vectors
[V,~,~]=stateVec(Henon,m,tau);

%-------------------------------------------------------------------------------------
fig1=figure(1);
plot(0:length(henon)-1,henon,'Color',[0.5,0.5,0.5],'LineWidth',4)
hold on;
plot(1,henon(2),'.r','MarkerSize',60);
plot(5,henon(6),'.r','MarkerSize',60);
plot(8,henon(9),'.r','MarkerSize',60);
plot(13,henon(14),'.r','MarkerSize',60);
plot(17,henon(18),'.r','MarkerSize',60);
hold off;
xlabel('$\mathbf{t}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex');
ylabel('$\mathbf{X(t)}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex');
grid on;
x(1)=0;
x(2)=20;
y(1)=-1.3;
y(2)=1.3;
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/6;
xs=(x(2)-x(1))/6;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
H=gca;
set(H,'LineWidth',3);
set(H,'GridLineStyle', '-');
set(H,'GridColor', 'k');
set(H,'GridAlpha', 0.9);
text(1.5, 1.1,'$$\mathbf{i_1}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(4, -0.18,'$$\mathbf{i_2}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(17.5, 1.1,'$$\mathbf{i_m}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');

%-------------------------------------------------------------------------------------
fig2=figure(2);
plot(henon,'Color',[0.5,0.5,0.5],'LineWidth',4)
hold on;
plot(7,henon(7),'.r','MarkerSize',60);
plot(16,henon(16),'.b','MarkerSize',60);
plot(10,henon(10),'.r','MarkerSize',60);
plot(19,henon(19),'.b','MarkerSize',60);
x(1)=5;
x(2)=20;
y(1)=-1.5;
y(2)=1.5;
temp=y(1):0.001:y(2);
temp2=repmat(13,1,length(temp));
plot(temp2,temp,'-.','Color',[0.2,0.2,0.2],'LineWidth',3);
hold off;
xlabel('$\mathbf{t}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex');
ylabel('$\mathbf{X(t)}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex');
grid on;
axis ([x(1) x(2) y(1) y(2)]);
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/3;
xs=(x(2)-x(1))/4;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
H=gca;
set(H,'LineWidth',3);
set(H,'GridLineStyle', '-');
set(H,'GridColor', 'k');
set(H,'GridAlpha', 0.9);
text(6.5, 1.1,'$$\mathbf{i_1}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(9.5, -0.3,'$$\mathbf{i_2}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(14.5, 1.1,'$$\mathbf{i_1}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
text(17.0, -0.3,'$$\mathbf{i_2}$$', 'interpreter','latex','FontSize', 34,'FontName', 'Helvetica');
% 
% % %-------------------------------------------------------------------------------------
% 
fig3=figure(3);
VT=V(1,:);
plot(V(:,2),V(:,1),'.','Color',[0.5,0.5,0.5,],'MarkerSize',20);
hold on;
plot(VT(1,2),VT(1,1),'.r','MarkerSize',60);
V(1,:)=[];
L=length(V);
D=max(abs(V-repmat(VT,L,1)),[],2);
ind=find(abs(D<0.05));
[~,i]=max(max(abs(V(ind,:)-repmat(VT,length(V(ind,:)),1)),[],2));
plot(V(ind(i),2),V(ind(i),1),'.b','MarkerSize',60);
hold off;
xlabel('$\mathbf{X_1}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex');
ylabel('$\mathbf{X_2}$','FontSize',25, 'FontName', 'Helvetica','Interpreter','Latex');
box on;
x(1)=0;
x(2)=1.5;
y(1)=-1;
y(2)=1;
axis ([x(1) x(2) y(1) y(2)]);
axis ([x(1) x(2) y(1) y(2)]);
ys=(y(2)-y(1))/3;
xs=(x(2)-x(1))/3;
set(gca,'XTick',x(1):xs:x(2))
set(gca,'YTick',y(1):ys:y(2))
grid on;
H=gca;
set(H,'LineWidth',3);
set(H,'GridLineStyle', '-');
set(H,'GridColor', 'k');
set(H,'GridAlpha', 0.9);
dock all;

end
