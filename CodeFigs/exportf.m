function exportf(fig_hand,fname,fpath)
fig=figure(fig_hand);


% conf figure
%set(fig,'FileName',fname);
set(fig,'Color',[1 1 1]);
set(fig, 'PaperType','A4');
set(fig, 'Units', 'centimeters')
set(fig, 'PaperUnits','centimeters');
%set(fig, 'WindowStyle','normal');

% remove white space when export
%xSize = 200; 
%ySize = 200;
%set(fig,'Position',[0 0 xSize ySize]);
pos = get(fig,'Position');
set(fig, 'PaperPositionMode', 'manual');
set(fig, 'PaperSize', [pos(3) pos(4)]);
set(fig, 'PaperPosition', [0 0 pos(3) pos(4)]);
% conf gca
set(gca,'FontName','helvetica');
set(gca,'FontSize',18);
%set(gca,'LineWidth',1);
%set(gca,'Position',[0.1 0.1 0.85 0.85]);

% Save
%print(fig,'-depsc','-r600',[fpath 'eps/' fname]);
%saveas(fig,[fpath 'fig/' fname],'fig');
print(fig, '-painters', '-dpdf','-r600',[fpath 'pdf/' fname]); 

end





