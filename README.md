------------------------------------------------------------------------------------------------------------------------
# PREDEFENSA DE TESIS DE DOCTORADO 
# Técnicas basadas en medidas de complejidad para el análisis de señales biomédicas
## Juan F. Restrepo
### jf.restrepo.rinckoar@bioingenieria.edu.ar
------------------------------------------------------------------------------------------------------------------------

### Objetivos

> * Proponer nuevos enfoques basados en teoría de la información con el fin de mejorar la capacidad de
    discriminación o segmentación de señales con diferente complejidad.

> *  Evaluar la capacidad de discriminación entre dinámicas de la entropía aproximada en base a sus parámetros y al
    nivel de ruido presente, tanto en señales reales como simuladas.
    
> * Explorar la  posible relación  entre la  estimación de  la suma de correlación y  el fenómeno  de resonancia
    estocástica.

### Día de presentación
#### 2015-03-10    

------------------------------------------------------------------------------------------------------------------------
### Log

#### 2015-02-04    
> * First commit

------------------------------------------------------------------------------------------------------------------------
